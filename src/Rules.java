import java.util.ArrayList;

public class Rules {

	public boolean general(Tile tile,int map[][],int x,int y,ArrayList<Tile> currentPlays){
		boolean temp=true;
		if((sixTiles(tile,map,x,y) ==true )&&(sequenceCol(tile,map,x,y)==true || sequenceShap(tile,map,x,y)==true)&& freeSpace(tile,map,x,y) == true && sameLine(currentPlays,map,x,y) ==true){			
		 temp=true;
		} else temp =false;
		return temp;
	}
	
	//at most six tiles placed next to each other
	public boolean sixTiles(Tile tile,int map[][],int x,int y){
		boolean temp=false;
		int newX=x;
		int newY=y;
		map=new int[newX][newY];
		int count =0;
		while(map != null){
			newX++;
			count++;
			map=new int[newX][y];
		}
		while(map != null){
			newX--;
			count++;
			map=new int[newX][y];
		}
		while(map != null){
			newY++;
			count++;
			map=new int[x][newY];
		}
		while(map != null){
			newY--;
			count++;
			map=new int[x][newY];
		}
		if(count <6){
			temp=true;
		}
		return temp;
	}
	//check if the sequence has the same color
	public boolean sequenceCol(Tile tile,int map[][],int x,int y){
		return true;
	}
	//check if the sequence has the same shape
	public boolean sequenceShap(Tile tile,int map[][],int x,int y){
		return true;
	}
	public static boolean checkEmpty(Tile[][] tile ){
		boolean temp=true;
	//	int x=0,y=0;
	//	while(tile[x][y] ==null )
		for(int i = 0; i < 17; i++){
			for(int j = 0; j < 13; j++){
				if (tile[i][j] != null ){
				temp=false;
				break;
				}
			}
		
	}
		System.out.println(temp);
		return temp;
		
	}
	//check if there is a free space to put the tile 
	public boolean  freeSpace(Tile tile,int map[][],int x,int y){
	//	map=new int[x][y];
		boolean temp=true;
		if(map!=null){
		temp=false;
		}
		return temp;
	}
	// check if the tles place are in the same line
	public boolean sameLine(ArrayList<Tile> currentPlays,int map[][],int x,int y){
		boolean temp = false;
		Tile[][] placeholder = new Tile[x][y];
		if(currentPlays.size() == 0){
			temp = true;
		} else if(currentPlays.size() == 1){
			if(placeholder [x+1][y] == currentPlays.get(0)){
				temp = true;
			}else if(placeholder[x-1][y] == currentPlays.get(0)){
				temp = true;
			}else if(placeholder[x][y+1] == currentPlays.get(0)){
				temp = true;
			}else if(placeholder[x][y-1] == currentPlays.get(0)){
				temp = true;
			}else{
				temp = false;
			}
		} else if(currentPlays.size() >= 2){
			if(placeholder[x+1][y] == currentPlays.get(currentPlays.size()-1) && placeholder[x+2][y] == currentPlays.get(currentPlays.size()-2)){
				temp = true;
			} else if(placeholder[x-1][y] == currentPlays.get(currentPlays.size()-1) && placeholder[x-2][y] == currentPlays.get(currentPlays.size()-2)){
				temp = true;
			} else if(placeholder[x][y+1] == currentPlays.get(currentPlays.size()-1) && placeholder[x][y+2] == currentPlays.get(currentPlays.size()-2)){
				temp = true;
			}else if(placeholder[x][y-1] == currentPlays.get(currentPlays.size()-1) && placeholder[x][y-2] == currentPlays.get(currentPlays.size()-2)){
				temp = true;
			}else{
				temp = false;
			}
		}
		return temp;
	}
	}

