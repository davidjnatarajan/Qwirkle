import java.util.ArrayList;


//This class will contain info about the player and their score


public class Player {
	
	ArrayList<Tile> hand = new ArrayList<>();
	int score = 0;
	protected int[][] plays = new int[13][27];
	
	public Player(ArrayList<Tile> tiles) {
		buildHand(tiles);
	}

	public void buildHand(ArrayList<Tile> a){
		for(int i=0;i<=5;i++){
			int randomIndex=(int)(Math.random() * a.size());
			this.hand.add((a.get(randomIndex)));
			QwirkleGUI.tileBag.remove(a.get(randomIndex));
		}
	}
	
	public void replaceTile(ArrayList<Tile> a) {
		int randomIndex = (int) (Math.random() * a.size());
		this.hand.add((a.get(randomIndex)));
		QwirkleGUI.tileBag.remove(a.get(randomIndex));
	}

}