import java.util.ArrayList;
import java.util.Optional;

import javafx.scene.control.Dialog;
import javafx.application.Application;
import javafx.scene.layout.Pane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by davidjnatarajan on 29/10/15.
 */

public class QwirkleGUI extends Application implements EventHandler<ActionEvent> {

	private ChoiceDialog<String> numberOfPlayersDialog;
    private Dialog<String> rulesDialog;
    private String rulesText;
    private ButtonType closeRules;
    private Button newGame;
    private Button rules;
    private Button swapTiles;
    private Button endTurn;
    private Label currentPlayerLabel;
    private ArrayList<Label> playerScoreLabels;
    private static Pane board;
    private VBox leftBox;
    private VBox rightBox;
    private static BorderPane layout;
    private static HBox hbox;
    public static ImageView target;
    private static ArrayList<Player> players = new ArrayList<>();
    private static int currentPlayer = 0;
    boolean isPlayer1 = true;
    boolean isPlayer2 = false;
    public static ArrayList<Tile> tileBag = new ArrayList<>();
    public Tile[][] positionReference = new Tile[21][15];
    private static ArrayList<ImageView> backBoard = new ArrayList<>();
    private static ArrayList<ImageView> placedTiles = new ArrayList<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
    	
    	playerScoreLabels = new ArrayList<>();
    	tileBag = Tile.tileGenerator(tileBag);
    	numberOfPlayersDialog = new ChoiceDialog<>("2", "2", "3", "4");
    	rulesDialog = new Dialog<>();
    	closeRules = new ButtonType("Close", ButtonData.CANCEL_CLOSE);
    	
    	rulesText = new String("Components: \n108 tiles, three of each of the 36 different tiles comprised of 6 colours (red, orange, yellow, green, blue, purple) \nand 6 shapes (circle, square, cross, star, diamond, clover)"
				+ "\n  \nObjective: \nStrategically create and expand lines of colour and shape in order to score the most points"	    
				+ "\n  \nPlay:  \nOn your turn do one of the three following things: \n1) Add one tile to the grid. You will automatically receive another tile. \n2) Add two or more tiles to the grid. All tiles played from your hand must share one attribute. Either colour or shape. "
				+ "\nYour tiles must be placed int the same line, although they do not have to touch each other.Your hand will automatically refill.\n3) Trade using the swap button some or all of your tiles for different tiles"
				+ "\n  \nAdding to the Grid: \nPlayers take turns adding to the grid. All tiles must connect to the grid. \nTwo or more tiles that touch create a line.\nA line is either all one shape or all one colour.\nTiles that are added to a line must share the same attribute as the tiles that are already on the line.\nOften there are places on the grid where no tile can be played."
				+ "\nA line of shapes  can only have one tile of each of the six colours. For example, a line of squares can only have one blue square.\nA line of colour can only have one tile of each of the six shapes.For example, a line of yellow can only have one yellow circle."
				+ "\n  \nTrading Tiles: \nOn your turn, you can choose to trade in some or all of your tiles instead of adding to the grid.\nSet aside the tiles you want to trade in. The amount of tiles you swapped will be replaced automatically.\nIf you are unable to add to the grid on your turn, you must trade in some or all of your tiles."
				+ "\n  \nScoring:\nWhen you create a line, you score one point for each tile in the line. \nAlso, when you add to an existing line, you score one point for each tile in that line, including tiles that were already in that line.\nOne tile can score two points if it is a part of two different lines.\nYou score an additional six points whenever you complete a line of six tiles, This is called a Qwirkle!"
				+ "\nA qwirkle scores at least 12 points. Six for the six tiles, and six for the bonus.\nThe six tiles must be either six tiles of the same colour and different shape OR six tiles of the same shape and different colour.\nLines of more than six tiles are not allowed.\nWhoever ends the game gets a six-point bonus");
    	
        primaryStage.setTitle("QWIRKLE");
        board = new Pane();
        leftBox = new VBox();
        rightBox = new VBox();
        hbox = new HBox();
        layout = new BorderPane();
        newGame = new Button("New Game");
        newGame.setPrefSize(100,30);
        rules = new Button("Rules");
        rules.setPrefSize(100, 30);
        swapTiles = new Button("Hand Swap");
        swapTiles.setPrefSize(100, 30);
        endTurn = new Button("End Turn");
        endTurn.setPrefSize(100, 30);
        
        numberOfPlayersDialog.setTitle("Welcome");
        numberOfPlayersDialog.setContentText("How many players?");
        numberOfPlayersDialog.showAndWait();
        Optional<String> result = numberOfPlayersDialog.showAndWait();
        if (result.isPresent()){
            if (result.get() == "2") {
            	createPlayers(2);
            } else if (result.get() == "3") {
            	createPlayers(3);
            } else if (result.get() == "4") {
            	createPlayers(4);
            }
        } else {
        	createPlayers(2);
        }

        for(int i = 0; i < 315; i++) {
            BackBoardTile bbt = new BackBoardTile();
            bbt.setPosition(i);
            backBoard.add(bbt.getNode());
        }

        board.getChildren().addAll(backBoard);
        
        currentPlayerLabel = new Label("Current player: " + (currentPlayer + 1));

        leftBox.setPadding(new Insets(210, 15, 15, 12));
        leftBox.setSpacing(25);
        leftBox.getChildren().add(currentPlayerLabel);
        leftBox.getChildren().addAll(playerScoreLabels);

        rulesDialog.setTitle("Rules");
        rulesDialog.setContentText(rulesText);
        rulesDialog.getDialogPane().getButtonTypes().add(closeRules);
        rulesDialog.getDialogPane().setPrefSize(800, 720);
        rules.setOnAction(this);
        newGame.setOnAction(this);
        endTurn.setOnAction(this);
        swapTiles.setOnAction(this);
       
        rightBox.setPadding(new Insets(210, 15, 15, 12));
        rightBox.setSpacing(25);
        rightBox.getChildren().addAll(newGame, rules, swapTiles, endTurn);

        hbox.setPadding(new Insets(15, 12, 15, 425));
        hbox.setSpacing(10);
        hbox.getChildren().addAll(players.get(currentPlayer).hand);

        layout.setLeft(leftBox);
        layout.setRight(rightBox);
        layout.setBottom(hbox);
        layout.setCenter(board);
        Scene scene = new Scene(layout, 1150, 700);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void placeTile(ImageView source) {
        for (int i = 0; i < players.get(currentPlayer).hand.size(); i++) {
            if (players.get(currentPlayer).hand.get(i).getNode() == source) {
                source.setFitWidth(40 - 1);
                source.setPreserveRatio(true);
                source.setX(20);
                source.setY(20);
                source.setTranslateX(target.getTranslateX());
                source.setTranslateY(target.getTranslateY());
                players.get(currentPlayer).hand.remove(i);
            	players.get(currentPlayer).replaceTile(tileBag);
                hbox.getChildren().remove(i);
                placedTiles.add(source);
                board.getChildren().add(source);
            }
        }
    }

    public static void removeBackBoard() {
        backBoard.remove(5);
    }
    
    public void createPlayers(int number) {
    	for (int i = 0; i < number; i++) {
    		players.add(new Player(tileBag));
    		playerScoreLabels.add(new Label("Player " + (i + 1) + " score: " + players.get(i).score));
    	}
    }
    
    public void endTurn() {
    	hbox.getChildren().clear();
    	if (currentPlayer < players.size() - 1) {
    		currentPlayer++;
    	} else {
    		currentPlayer = 0;
    	}
    	hbox.getChildren().addAll(players.get(currentPlayer).hand);
    	currentPlayerLabel.setText("Current player: " + (currentPlayer + 1));
    }
    
    public static void swapTile(ImageView source) {
    	for (int i = 0; i < players.get(currentPlayer).hand.size(); i++) {
            if (players.get(currentPlayer).hand.get(i).getNode() == source) {
                players.get(currentPlayer).hand.remove(i);
            	players.get(currentPlayer).replaceTile(tileBag);
                hbox.getChildren().remove(i);
            }
        }
    }
    
    public void swapHand() {
    	while (hbox.getChildren().isEmpty() == false) {
            players.get(currentPlayer).hand.remove(0);
        	players.get(currentPlayer).replaceTile(tileBag);
            hbox.getChildren().remove(0);
        }
    }

    //When button is clicked, handle() gets called
    //Button click is an ActionEvent (also MouseEvents, TouchEvents, etc...)
    @Override
    public void handle(ActionEvent event) {
        if (event.getSource() == newGame) {
            System.out.println("Hey Charlie!");
        } else if (event.getSource() == rules) {
        	rulesDialog.showAndWait();
        } else if (event.getSource() == endTurn) {
        	endTurn();
        } else if (event.getSource() == swapTiles) {
        	swapHand();
        }
    }

}
