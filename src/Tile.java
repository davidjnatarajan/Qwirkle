import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;

import java.util.ArrayList;

/**
 * Created by Aleksandra on 2015-10-31.
 */

public class Tile extends Parent{
  //  private static final String Get1 = null;
	private final int key;
    private final int col;
    private final int shape;
	private final Image tileImage;
	private final ImageView currentTile;

    public Tile(int key, int col, int shape) {
        this.key = key;
        this.col = col;
        this.shape = shape;
		tileImage = new Image(getReference(col, shape));
		currentTile = new ImageView(tileImage);
		getChildren().add(currentTile);
		currentTile.setOnDragDetected(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				Dragboard db = currentTile.startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				content.putString("This has been dropped");
				db.setContent(content);
				db.setDragView(tileImage);
				event.consume();
			}
		});
		currentTile.setOnDragDone(new EventHandler<DragEvent>() {
			public void handle(DragEvent event) {
				if (event.getTransferMode() == TransferMode.COPY) {
					System.out.println("Drag is done");
					QwirkleGUI.placeTile(currentTile);
				}
				event.consume();
			}
		});
		currentTile.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				QwirkleGUI.swapTile(currentTile);
			}
		});
	}

    public static ArrayList<Tile> tileGenerator(ArrayList<Tile> a) {
    int k=0;
        for (int i = 0; i <=5; i++) {
            for (int j = 0; j <= 5; j++) {
                a.add(new Tile(k, i, j));
                a.add(new Tile(k+1, i, j));
                a.add(new Tile(k+2, i, j));
               	int c=k+2;
               	c++;
               	k=c;
            }
        }
        return a;
    }

	public ImageView getNode() {
		return currentTile;
	}

    int getCol() {
		return this.col;
	}

	int getKey() {
		return this.key;
	}

	int getShape(){
		return this.shape;
	}
    	
	public static String getReference( int color, int shape) {
		if(color == 0 ) {
			if( shape == 0) {
				return "BlueSquareTile.png";
			}
			else if( shape == 1)
			{
				return "BlueCircleTile.png";
			}
			else if( shape == 2)
			{
				return "BlueDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "BlueStarTile.png";
			}
			else if( shape == 4)
			{
				return "BlueCrossTile.png";
			}
			else if( shape == 5)
			{
				return "BlueCloverTile.png";
			}
		}
		else if(color == 1 )
		{
			if( shape == 0) {
				return "PurpleSquareTile.png";
			}
			else if( shape == 1)
			{
				return "PurpleCircleTile.png";
			}
			else if( shape == 2)
			{
				return "PurpleDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "PurpleStarTile.png";
			}
			else if( shape == 4)
			{
				return "PurpleCrossTile.png";
			}
			else if( shape == 5)
			{
				return "PurpleCloverTile.png";
			}
		}
		else if(color == 2 )
		{
			if( shape == 0)
			{
				return "RedSquareTile.png";
			}
			else if( shape == 1)
			{
				return "RedCircleTile.png";
			}
			else if( shape == 2)
			{
				return "RedDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "RedStarTile.png";
			}
			else if( shape == 4)
			{
				return "RedCrossTile.png";
			}
			else if( shape == 5)
			{
				return "RedCloverTile.png";
			}
		}
		else if(color == 3 )
		{
			if( shape == 0)
			{
				return "OrangeSquareTile.png";
			}
			else if( shape == 1)
			{
				return "OrangeCircleTile.png";
			}
			else if( shape == 2)
			{
				return "OrangeDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "OrangeStarTile.png";
			}
			else if( shape == 4)
			{
				return "OrangeCrossTile.png";
			}
			else if( shape == 5)
			{
				return "OrangeCloverTile.png";
			}
		}
		else if(color == 4 )
		{
			if( shape == 0)
			{
				return "YellowSquareTile.png";
			}
			else if( shape == 1)
			{
				return "YellowCircleTile.png";
			}
			else if( shape == 2)
			{
				return "YellowDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "YellowStarTile.png";
			}
			else if( shape == 4)
			{
				return "YellowCrossTile.png";
			}
			else if( shape == 5)
			{
				return "YellowCloverTile.png";
			}
		}
		else if(color == 5 )
		{
			if( shape == 0)
			{
				return "GreenSquareTile.png";
			}
			else if( shape == 1)
			{
				return "GreenCircleTile.png";
			}
			else if( shape == 2)
			{
				return "GreenDiamondTile.png";
			}
			else if( shape == 3)
			{
				return "GreenStarTile.png";
			}
			else if( shape == 4)
			{
				return "GreenCrossTile.png";
			}
			else if( shape == 5)
			{
				return "GreenCloverTile.png";
			}
		}
		return null;
	}
}