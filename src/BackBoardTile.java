import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

/**
 * Created by davidjnatarajan on 02/11/15.
 */
public class BackBoardTile extends Parent{

    private Image backBoardTileImage = new Image("BlackTile.png");
    private ImageView currentTile = new ImageView(backBoardTileImage);

    public BackBoardTile() {
        currentTile.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if ( db.hasString() )
                {
                    event.acceptTransferModes( TransferMode.COPY_OR_MOVE );
                }
                event.consume();
            }
        });
        currentTile.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {
                    success = true;
                    System.out.println(db.getString());
                    QwirkleGUI.target = currentTile;
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
    }

    public ImageView getNode() {
        return currentTile;
    }

    public void setPosition(int arrayPosition) {
        currentTile.setFitWidth(40 - 1);
        currentTile.setPreserveRatio(true);
        currentTile.setX(40 / 2);
        currentTile.setY(40 / 2);
        currentTile.setTranslateX((arrayPosition % 21) * 40);
        currentTile.setTranslateY((arrayPosition / 21) * 40);
    }

}
